/* 
    npm init
        - triggering this command will prompt user for different settings that will define the application
        - using this command will make a "package.json" in our repo
        - package.json tracks the version of our application, depending on the settings we have set; we can also see the dependencies that we have installed inside of this file

    npm install express
        - after triggering this command, express will now be listed as a "dependency"; this can be seen inside the package.json file under the "dependencies" property
        
        - installing any dependency using npm will result into creating "node_modules" folder and package-lock.json

        
    "node_modules" 
        - where the dependencies needed files are stored
        - directory should be left on the local repository because some of the hosting sites will fail to load the repository once it found the "node_modules" inside the repo
        - left on local repository because it takes too much time to commit

    .gitignore
        - a file that tells git what files will be ignored when committing/pushing

    npm install
        - used when there are available dependencies inside our package.json but are not yet installed inside the repo/project
        - useful when cloning repositories to our local repository
*/

// import express module
const e = require("express");
const express = require("express");
// express() - create a server using express, and store in app variable
const app = express();
// setup port
const port = 3000;

/* 
    app.use()
        - let server handle data from requests
*/

// allows app to read and handle json requests
app.use(express.json());

// allows the server to read data from ports
// by default, information received from url can only be received as a string or an array; but using extended:true for the argument allows to receive information from other data types such as objects
app.use(express.urlencoded({extended: true}));

// tell server to listen to port 3000
// if port is accessed, we can run the server
// returns a message to confirm that the server is running
app.listen(port, () => console.log(`Server running at port: ${port}`));

/* 
    middleware
        - software that provide common services for the application
*/


// SECTION - ROUTES
/* 
    Express has methods corresponding to each HTTP methods
        - the route below expects to receive a GET request at the base URI "/"
*/
// GET method
app.get("/", (req, res) => {
    res.send("Hello World");
});

// mini-activity
app.get("/hello", (req, res) => 
{
    res.send("Hello from /hello endpoint");
});

// POST method
// This route expects to receive a POST request at the URI "/hello"
app.post("/hello", (req, res) => 
{
    // req.body contains the contents/data of the request body
    // the properties defined in Postman request will be accessible here as propertes with the same name
    res.send(`Hello there ${req.body.firstName} ${req.body.lastName}!`);
});

let users = [
    {
        firstName: "Pocket",
        lastName: "Sage"
    },
    {
        firstName: "Peter",
        lastName: "Parker"
    },
    {
        firstName: "Cardo",
        lastName: "Dalisay"
    }
];

app.post("/signup", (req, res) => 
{
    if (req.body.firstName !== "" && req.body.lastName !== "") 
    {
        users.push(req.body);
        res.send(`The user ${req.body.firstName} ${req.body.lastName} signed up successfully!`)
        console.log(users)
    }  
    else 
    {
        res.send(`Please input both first name and last name`);
    }
})

app.put("/change-lastName", (req,res) => 
{
    let message;

    for (let i = 0; i < users.length; i++) {
        if (req.body.firstName === users[i].firstName) 
        {
            users[i].lastName = req.body.lastName;
            message = `User ${req.body.firstName} has successfully changed the last name into ${req.body.lastName}`;

            console.log(users)
            break;
        }
        else
        {
            message = "User does not exist"
        }
    }

    res.send(message);
});





// SECTION - ACTIVITY



// GET - /home route
app.get("/home", (req, res) =>
{
    res.send(`Welcome to the home page`);
});

// GET - /users route
app.get("/users", (req, res) => 
{
    res.send(users);
});

// DELETE - /delete-user route
app.delete(`/delete-user`, (req, res) =>
{
    // find correct user from users mock database
    const user = users.find(el => el.firstName === req.body.firstName);
    // remove user from users database (which is an array for now)
    users.splice(user.firstName, 1);
    // create new array for remaining users
    let remainingUsers = [];
    // add remaining users to remainingUsers array
    users.forEach(user => remainingUsers.push(`${user.firstName} ${user.lastName}`));

    //output
    res.send(`Deleted user ${user.firstName} ${user.lastName}. 
        \n Remaining users: ${remainingUsers.join(', ')}`);
});